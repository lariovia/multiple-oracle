using JuMP
using PATHSolver
using LinearAlgebra


""" Finds a nash equilibrium of a game given by two payoff matrices. """
function nash_equilibrium(A, B; offset=0, optimizer=PATHSolver.Optimizer)
	A, B = -1 * A, -1 * B
	m, n = axes(A)

	# This formulation requires positive loss matrices
	Ap = A .- minimum(A) .+ (1 + offset)
	Bp = B .- minimum(B) .+ (1 + offset)

	model = Model(optimizer)

	@variable model x[m] >= 0
	@variable model y[n] >= 0
	@constraint(model, Ap  * y .- 1 ⟂ x)
	@constraint(model, Bp' * x .- 1 ⟂ y)

	set_silent(model)
	optimize!(model)

	xs = value.(x) / sum(value.(x))
	ys = value.(y) / sum(value.(y))

	-[dot(xs, A, ys), dot(xs, B, ys)], [xs, ys]
end 


# prisoner_A = [-2 -10; 
			  #  0 -5]
# prisoner_B = [ -2  0; 
# 			    -10 -5]

# values, strategies = nash_equilibrium(prisoner_A, prisoner_B)

# @show values
# @show strategies