using JuMP 
import Ipopt
include("bimatrix.jl")

# Example 3
# function create_payoff_matrices(X::Vector, Y::Vector)
#     A = [u1(x, y) for x in X, y in Y]
#     B = [u2(x, y) for x in X, y in Y]

#     A, B
# end 

function create_payoff_matrices(X::Vector, Y::Vector)
    A = [J1(x, y) for x in X, y in Y]
    B = [J2(y, x) for x in X, y in Y]

    -A, -B
end 

function display_strategy(strategy)
    for (prob, sup) in strategy
            if isapprox(prob, 0.0)
                    continue
            end
    sup_str = round(sup; digits=3)
    prob_str = round(prob * 100; digits=3)
            println("\t $sup_str with prob $prob_str%")
    end
end

#init payoff functions
sigma = 1
c1(lambda) = lambda^1.01
c2(lambda) = lambda^20
f(lambda1, lambda2) = 1/(lambda1 + lambda2) + sigma/lambda1 + sigma/lambda2
J1(lambda1, lambda2) = c1(lambda1) + f(lambda1, lambda2)
J2(lambda1, lambda2) = c2(lambda1) + f(lambda1, lambda2)
# Example 3
# alpha_1 = 1.0
# alpha_2 = 1.5
# phi_1 = 0.0
# phi_2 = pi / 8
# u1(t1, t2) = alpha_1 * cos(t1 - phi_1) - cos(t1 - t2)
# u2(t1, t2) = alpha_2 * cos(t2 - phi_2) - cos(t2 - t1)

# init first step
pure_1 = rand(1) # <0, 1>
pure_2 = rand(1) # <0, 1>
strategies = [zip([1.0], pure_1), zip([1.0], pure_2)]

#init model Ipopt
model = Model(Ipopt.Optimizer)
register(model, :J1, 2, J1; autodiff=true)
register(model, :J2, 2, J2; autodiff=true)
@variable(model, 1/sigma^2 >= x >= 0) 
set_silent(model)
# for Example 3
# register(model, :u1, 2, u1; autodiff=true)
# register(model, :u2, 2, u2; autodiff=true)
# @variable(model, pi >= x >= -pi)

# oracle
for j in 1:100
    # find pure strategy for each player and save it
    # for player one
    # @NLobjective(model, Max, sum(u1(x, value) * probability for (probability, value) in strategies[2]))
    @NLobjective(model, Min, sum(J1(x, value) * probability for (probability, value) in strategies[2]))
    optimize!(model)
    union!(pure_1, value(x))
    # for player two
    # @NLobjective(model, Max, sum(u2(value, x) * probability for (probability, value) in strategies[1]))
    @NLobjective(model, Min, sum(J2(x, value) * probability for (probability, value) in strategies[1]))
    optimize!(model)
    union!(pure_2, value(x))

    payoff_A, payoff_B = create_payoff_matrices(pure_1, pure_2)
    # find mixed strategies 
    _, s = nash_equilibrium(payoff_A, payoff_B) # s[i] - mixed strategy P_i
    strategies[1] = zip(s[1], pure_1) # zip(P_1, X_1)
    strategies[2] = zip(s[2], pure_2) # zip(P_2, X_2)
end

println("Strategy of player 1:")
display_strategy(strategies[1])
println("Strategy of player 2:")
display_strategy(strategies[2])