using JuMP 
import Ipopt

steps = 10
a = 1; b = 10 # interval
# init first step
responses = zeros(Float16, 2, steps+1)
responses[1, 1] = rand(a:b)
responses[2, 1] = rand(a:b)

# market 
c = 1 # cost price
d = 10 # size of market
u(x, y) = 
        if(x + y < d) x*(d-x-y-c)
        else -x*c
        end

#init model Ipopt
model = Model(Ipopt.Optimizer)
register(model, :u, 2, u; autodiff=true)
@variable(model, b >= x >= a)
set_silent(model)

for k in 2:steps+1
    # find best response
    @NLobjective(model, Max, u(x, responses[2, k-1]))
    optimize!(model)
    responses[1, k] = value(x)
    @NLobjective(model, Max, u(x, responses[1, k-1]))
    optimize!(model)
    responses[2, k] = value(x)
end

# answer = (d-c)/3 = (10-1)/3 = 3
println("Market")
println(responses[1, :])
println(responses[2, :])
